package challange4;

public class main {
    public static void main(String[] args) {
        Herbivor h = new Herbivor("Kambing","tumbuhan","tumpul");
        h.IdentifyMyself(h);
        Carnivor c = new Carnivor("Kambing","tumbuhan","tumpul");
        c.IdentifyMyself(c);
        Omnivor o = new Omnivor("Ayam","semua","tajam dan tumpul");
        o.IdentifyMyself(o);
    }
}
