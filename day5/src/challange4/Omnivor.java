package challange4;

public class Omnivor extends Animal{
    String Makanan;
    String value;
    Omnivor(String name, String makanan, String gigi){
        super(name,"Omnivor",gigi);
        this.Makanan = makanan;
    }
    void IdentifyMyself(Omnivor omnivor){

        System.out.println("Hi I'm "+omnivor.Jenis+" My Name is " + omnivor.Nama + ", My Food is"+" '"+omnivor.Makanan+"',"+" I have "+omnivor.Gigi+" teeth");
    }
}
