package challange4;

public class Herbivor extends Animal
{
    String Makanan;

    Herbivor(String name,  String makanan, String gigi){
        super(name,"Herbivor",gigi);
        this.Makanan = makanan;
    }
    void IdentifyMyself(Herbivor herbivor){

        System.out.println("Hi I'm "+herbivor.Jenis+", My Name is " + herbivor.Nama + ", My Food is"+" '"+herbivor.Makanan+"',"+" I have "+herbivor.Gigi+" teeth");
    }
}
