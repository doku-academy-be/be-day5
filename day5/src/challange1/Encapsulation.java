package challange1;

public class Encapsulation {
    public static void main(String[] args) {

        System.out.println("challange1.Cat");
        Cat cat = new Cat();
        cat.ShowIdentity("Hitam",4);
        cat.ShowIdentity("Putih",3);
        cat.ShowIdentity("Hitam Putih",4);
        cat.ShowIdentity("Poleng poleng",3);
        cat.ShowIdentity("bintik bintik",4);

        System.out.println("challange1.Fish");
        Fish fish = new Fish();
        fish.ShowIdentity("paus","plankton");
        fish.ShowIdentity("cupang","cacing");
        fish.ShowIdentity("arwan","jangkrik");
        fish.ShowIdentity("sapu-sapu","pelet");

        System.out.println("challange1.Flower");
        Flower flower = new Flower();
        flower.ShowIdentity("bangkai","merah",12);
        flower.ShowIdentity("anggrek","putih",8);
        flower.ShowIdentity("mawar","merah",3);
        flower.ShowIdentity("melati","kuning",5);

        System.out.println("challange1.Car");
        Car car = new Car();
        car.ShowIdentity("sedan","merah",4);
        car.ShowIdentity("truk","hijau",6);
        car.ShowIdentity("tronton","coklat",12);
        car.ShowIdentity("angkot","kuning",4);
    }
}