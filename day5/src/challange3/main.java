package challange3;

public class main {
    public static void main(String[] args) {

        Bikes pedal = new Bikes("Pedal Bike","no Engine",2);
        Bikes motor = new Bikes("Motor Bike","with Engine",2);

        System.out.println("Hi I'm Bike, My Name is " + pedal.name + " Status is " + pedal.withEngine + ", I have " + pedal.wheelCount + " wheel(s)");
        System.out.println("Hi I'm Bike, My Name is " + motor.name + " Status is " + motor.withEngine + ", I have " + motor.wheelCount + " wheel(s)");

        Cars sport = new Cars("Sport Cars","with engine",4);
        Cars pickup = new Cars("Pickup Cars","with engine",4);
        System.out.println("Hi I'm Cars, My Name is " + sport.name + " Status is " + sport.withEngine + ", I have " + sport.wheelCount + " wheel(s)");
        System.out.println("Hi I'm Cars, My Name is " + pickup.name + " Status is " + pickup.withEngine + ", I have " + pickup.wheelCount + " wheel(s)");

        Buses transjakarta = new Buses("[Private Bus]","Transjakarta","with Engine",4);
        Buses schoolbus = new Buses("[Public Bus]","School Bus","with Engine",4);
        System.out.println("Hi I'm Bus "+ schoolbus.privatebus +", My Name is " + schoolbus.name + " Status is " + schoolbus.withEngine + ", I have " + transjakarta.wheelCount + " wheel(s)");
        System.out.println("Hi I'm Bus "+ transjakarta.privatebus +", My Name is " + transjakarta.name + " Status is " + transjakarta.withEngine + ", I have " + transjakarta.wheelCount + " wheel(s)");


    }
}
