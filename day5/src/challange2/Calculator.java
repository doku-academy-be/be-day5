package challange2;

public abstract class Calculator {
    int val1;
    int val2;
    public abstract void Sum();
    public abstract void Sub();
    public abstract void Multiply();
    public abstract void Divide();
}
