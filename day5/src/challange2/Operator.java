package challange2;

import challange2.Calculator;

public class Operator extends Calculator {
    int val1;
     int val2;

    public Operator(int val1,int val2){
        this.val1 = val1;
        this.val2 = val2;
    };
    @Override
    public void Sum() {

       int result = val1+val2;
        System.out.println("Hasil: "+ result);

    }

    @Override
    public void Sub() {
        int result = val1-val2;
        System.out.println("Hasil: "+ result);
    }

    @Override
    public void Multiply() {
        int result = val1*val2;
        System.out.println("Hasil: "+ result);
    }

    @Override
    public void Divide() {
        int result = val1/val2;
        System.out.println("Hasil: "+ result);
    }


}
